// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ChangeColorMaterialMeshOrange.generated.h"

UCLASS()
class PAINT_SPLASH_MARTIN_API AChangeColorMaterialMeshOrange : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AChangeColorMaterialMeshOrange();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// The main static mesh body.
	UPROPERTY(VisibleAnywhere)
	class UStaticMeshComponent* MyMesh;

	// The material that will be displayed when the BoxComponent is overlapped.
	UPROPERTY(EditAnywhere)
	class UMaterial* OnMaterial;

	// The material that will be displayed when the game starts.
	UPROPERTY(EditAnywhere)
	class UMaterial* OffMaterial;

	// A box component to check for overlapping actors.
	UPROPERTY()
	class UBoxComponent* MyBoxComponent;

	// A function that will run when the box component is overlapped.
	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
