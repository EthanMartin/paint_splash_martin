// Fill out your copyright notice in the Description page of Project Settings.


#include "ChangeColorMaterialMeshOrange.h"
#include "DrawDebugHelpers.h"
#include "Components/BoxComponent.h"

// Sets default values
AChangeColorMaterialMeshOrange::AChangeColorMaterialMeshOrange()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Creates the SubObject of the static mesh and sets it as the root componenet.
	MyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MyMesh"));
	RootComponent = MyMesh;

	// Establishes the basic characteristics of the box component.
	MyBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("MyBoxComponent"));
	MyBoxComponent->InitBoxExtent(FVector(100, 100, 100));
	MyBoxComponent->SetCollisionProfileName("Trigger");
	MyBoxComponent->SetupAttachment(RootComponent);

	// Creates the SubObjects for both the OnMaterial(Orange) and the OffMaterial(Blue).
	OnMaterial = CreateDefaultSubobject<UMaterial>(TEXT("GlowingOrange"));
	OffMaterial = CreateDefaultSubobject<UMaterial>(TEXT("GlowingBlue"));

	// A function that will run when the box component is overlapped.
	MyBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AChangeColorMaterialMeshOrange::OnOverlapBegin);
}

// Called when the game starts or when spawned
void AChangeColorMaterialMeshOrange::BeginPlay()
{
	Super::BeginPlay();
	
	// Sets the mesh's base color to Blue
	MyMesh->SetMaterial(0, OffMaterial);
}

// Called every frame
void AChangeColorMaterialMeshOrange::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

}

// Called when an actor overlaps the box component.
void AChangeColorMaterialMeshOrange::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		// Changes the material color from Blue to Orange.
		MyMesh->SetMaterial(0, OnMaterial);
	}
}

