// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "RuntimeMeshActor.h"
#include "ChangeMaterialMeshActor.generated.h"

/**
 * 
 */
 //!!!!!!!!!!!!!
 // Currently Unused
 //!!!!!!!!!!!!!
UCLASS()
class PAINT_SPLASH_MARTIN_API AChangeMaterialMeshActor : public ARuntimeMeshActor
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:
	UMaterialInterface* Material;

public:
	AChangeMaterialMeshActor();

	void OnConstruction(const FTransform& Transform) override;

	UPROPERTY(EditAnywhere)
	class UMaterial* OffMaterial;

private:
	virtual void GenerateBoxMesh();
	virtual void CreateBoxMesh(FVector BoxRadius,
		TArray<FVector>& Vertices,
		TArray<int32>& Triangles,
		TArray<FVector>& Normals,
		TArray<FVector2D>& UVs,
		TArray<FRuntimeMeshTangent>& Tangents,
		TArray<FColor>& Colors);
};
